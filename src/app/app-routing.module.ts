import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {AddStudentComponent} from './add-student/add-student.component';
import {StudentsListComponent} from './student-list/student-list.component';
import {EditStudentComponent} from './edit-student/edit-student.component';

const routes: Routes = [
  {path: '', redirectTo: '/register-student', pathMatch: 'full'},
  {path: 'register-student', component: AddStudentComponent},
  {path: 'view-students', component: StudentsListComponent},
  {path: 'edit-student/:id', component: EditStudentComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {

}
